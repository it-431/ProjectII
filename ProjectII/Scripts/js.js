$("[name='toggleMenu']").click(toggleMenu);

function toggleMenu() {
    $("nav").toggleClass("open");
    $("#nav-wrapper").toggleClass("overlay");
    $("#open").toggle();
}

$(window).click(function (e) {
    if ($(e.target).attr("class") === "overlay") {
        toggleMenu();
    }
});

flatpickr($("#GraduationDate"), {
    altInput: true,
    altFormat: "F j, Y"
});

flatpickr($("#DOB"), {
    altInput: true,
    altFormat: "F j, Y",
    maxDate: "today"
});

$("#DOB + input").attr("name", "DOB");

$('#EnrollmentYear:not([readonly])').yearselect({
    start: new Date().getFullYear(),
    end: add_years(new Date(), 25).getFullYear()
});

function add_years(date, y) {
    return new Date(date.setFullYear(date.getFullYear() + y));
}

$(".dropdown").dropdown();

$("#SSN").on("input", function (e) {
    var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,2})(\d{0,4})/);
    e.target.value = !x[2] ? x[1] : x[1] + '-' + x[2] + (x[3] ? '-' + x[3] : '');
});

$("input[type='tel']").on("input", function (e) {
    var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
    e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
});

$("#open").click(function () {
    toggleNav();
})

function toggleNav() {
    $("nav").toggleClass("open");
    $(".icon-bar").toggleClass("open-icon");
    $("header img").toggle();
}

$(window).click(function(e) {
    if (e.target.tagName.toLowerCase() == "main" && $(window).width() < 1000) {
        $("nav").removeClass("open");
    }
});