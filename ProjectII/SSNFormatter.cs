﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectII
{
    public static class SSNFormatter
    {
        public static string MaskSSN(string SSN)
        {
            // Check for empty string.  
            if (string.IsNullOrEmpty(SSN))
            {
                return string.Empty;
            }

            var maskedSSN = "";

            while (maskedSSN.Length < SSN.Length - 4)
            {
                maskedSSN += "*";
            };

            maskedSSN += SSN.Substring(SSN.Length - 4);

            return maskedSSN;
        }
    }
}