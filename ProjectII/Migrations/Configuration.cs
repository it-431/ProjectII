﻿namespace ProjectII.Migrations
{
    using ProjectII.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ProjectII.Models.ProjectIIContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = "ProjectII.Models.ProjectIIContext";
        }

        protected override void Seed(ProjectII.Models.ProjectIIContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.

            if (!context.Applicants.Any())
            {
                var applicants = new List<Applicant>
                {
                    new Applicant() {
                        ApplicationDate = DateTime.Now.AddDays(-(int)DateTime.Today.DayOfWeek + (int)DayOfWeek.Monday),
                        FirstName = "Elle",
                        MiddleName = "L.",
                        LastName = "Brookes",
                        SSN = "578-94-8537", 
                        EmailAddress = "ebrookes27@gmail.com",
                        State = "DC",
                        DOB = new DateTime(2000, 8, 18),
                        Gender = "F",
                        GPA = 3.0M,
                        MathScore = 500,
                        VerbalScore = 600,
                        EnrollmentTerm = "Fall",
                        EnrollmentYear = DateTime.Now.Year
                    },
                    new Applicant() {
                        ApplicationDate = DateTime.Now,
                        FirstName = "Jennie",
                        LastName = "Elkie",
                        SSN = "126-24-1636", 
                        EmailAddress = "jelkie21@gmail.com",
                        State = "NY",
                        DOB = new DateTime(1999, 8, 18),
                        Gender = "F",
                        GPA = 3.4M,
                        MathScore = 650,
                        VerbalScore = 650,
                        EnrollmentTerm = "Spring",
                        EnrollmentYear = DateTime.Now.Year
                    },
                    new Applicant() {
                        ApplicationDate = DateTime.Now.AddDays(-(int)DateTime.Today.DayOfWeek + (int)DayOfWeek.Monday),
                        FirstName = "Olivia",
                        MiddleName = "E.",
                        LastName = "Hessler",
                        SSN = "106-40-6253", 
                        EmailAddress = "oliviah17@gmail.com",
                        State = "NY",
                        DOB = new DateTime(2000, 5, 17),
                        Gender = "F",
                        GPA = 3.0M,
                        MathScore = 500,
                        VerbalScore = 600,
                        EnrollmentTerm = "Fall",
                        EnrollmentYear = DateTime.Now.Year
                    },
                    new Applicant() {
                        ApplicationDate = DateTime.Now.AddDays(-(int)DateTime.Today.DayOfWeek + (int)DayOfWeek.Tuesday),
                        FirstName = "Elise",
                        MiddleName = "L.",
                        LastName = "Kim",
                        SSN = "008-05-9445",
                        EmailAddress = "ekim18@yahoo.com",
                        State = "VT",
                        DOB = new DateTime(1998, 1, 22),
                        Gender = "F",
                        GPA = 3.0M,
                        MathScore = 500,
                        VerbalScore = 600,
                        EnrollmentTerm = "Summer",
                        EnrollmentYear = DateTime.Now.Year
                    },
                    new Applicant() {
                        ApplicationDate = DateTime.Now,
                        FirstName = "Ben",
                        LastName = "Smith",
                        SSN = "579-10-0101", 
                        EmailAddress = "bsmith19@gmail.com",
                        State = "DC",
                        DOB = new DateTime(1997, 6, 12),
                        Gender = "M",
                        GPA = 3.0M,
                        MathScore = 500,
                        VerbalScore = 600,
                        EnrollmentTerm = "Fall",
                        EnrollmentYear = DateTime.Now.Year
                    },
                    new Applicant() {
                        ApplicationDate = DateTime.Now.AddDays(-(int)DateTime.Today.DayOfWeek),
                        FirstName = "Eric",
                        LastName = "Williams",
                        SSN = "009-38-3430", 
                        EmailAddress = "ewilliams17@gmail.com",
                        State = "VT",
                        DOB = new DateTime(1996, 11, 21),
                        Gender = "M",
                        GPA = 3.2M,
                        MathScore = 600,
                        VerbalScore = 600,
                        EnrollmentTerm = "Spring",
                        EnrollmentYear = DateTime.Now.Year
                    },
                    new Applicant() {
                        ApplicationDate = DateTime.Now.AddDays(-(int)DateTime.Today.DayOfWeek),
                        FirstName = "Jessie",
                        LastName = "Butterworth",
                        SSN = "215-27-3585", 
                        EmailAddress = "jbutterworth7@gmail.com",
                        State = "MD",
                        DOB = new DateTime(1999, 10, 18),
                        Gender = "F",
                        GPA = 3.2M,
                        MathScore = 600,
                        VerbalScore = 500,
                        EnrollmentTerm = "Fall",
                        EnrollmentYear = DateTime.Now.Year
                    },
                    new Applicant() {
                        ApplicationDate = DateTime.Now.AddDays(-(int)DateTime.Today.DayOfWeek + (int)DayOfWeek.Monday),
                        FirstName = "Lani",
                        MiddleName = "M.",
                        LastName = "Greene",
                        SSN = "090-76-1500", 
                        EmailAddress = "lmgreene21@gmail.com",
                        State = "NY",
                        DOB = new DateTime(2000, 12, 21),
                        Gender = "F",
                        GPA = 3.5M,
                        MathScore = 800,
                        VerbalScore = 600,
                        EnrollmentTerm = "Fall",
                        EnrollmentYear = DateTime.Now.Year
                    },
                    new Applicant() {
                        ApplicationDate = DateTime.Now,
                        FirstName = "Cherry",
                        LastName = "Brookes",
                        SSN = "216-57-2807",
                        EmailAddress = "cherryb20@gmail.com",
                        State = "MD",
                        DOB = new DateTime(1997, 7, 15),
                        Gender = "F",
                        GPA = 3.0M,
                        MathScore = 600,
                        VerbalScore = 500,
                        EnrollmentTerm = "Spring",
                        EnrollmentYear = DateTime.Now.Year
                    },
                    new Applicant() {
                        ApplicationDate = DateTime.Now,
                        FirstName = "John",
                        LastName = "Miller",
                        SSN = "231-15-6622",
                        EmailAddress = "jmiller@gmail.com",
                        State = "VA",
                        DOB = new DateTime(2000, 12, 19),
                        Gender = "M",
                        GPA = 3.2M,
                        MathScore = 600,
                        VerbalScore = 600,
                        EnrollmentTerm = "Fall",
                        EnrollmentYear = DateTime.Now.Year
                    },
                    new Applicant() {
                        ApplicationDate = DateTime.Now.AddDays(-(int)DateTime.Today.DayOfWeek),
                        FirstName = "Tim",
                        MiddleName = "E.",
                        LastName = "Davis",
                        SSN = "223-76-2037",
                        EmailAddress = "tdavis@gmail.com",
                        State = "VA",
                        DOB = new DateTime(2000, 8, 18),
                        Gender = "M",
                        GPA = 3.72M,
                        MathScore = 600,
                        VerbalScore = 800,
                        EnrollmentTerm = "Fall",
                        EnrollmentYear = DateTime.Now.Year
                    },
                    new Applicant() {
                        ApplicationDate = DateTime.Now.AddDays(-(int)DateTime.Today.DayOfWeek),
                        FirstName = "Paul",
                        LastName = "Wilson",
                        SSN = "229-82-8476",
                        EmailAddress = "pwilson@gmail.com",
                        State = "VA",
                        DOB = new DateTime(2000, 7, 22),
                        Gender = "M",
                        GPA = 3.0M,
                        MathScore = 500,
                        VerbalScore = 600,
                        EnrollmentTerm = "Summer",
                        EnrollmentYear = DateTime.Now.Year
                    },
                    new Applicant() {
                        ApplicationDate = DateTime.Now.AddDays(-(int)DateTime.Today.DayOfWeek),
                        FirstName = "Jennifer",
                        LastName = "Greene",
                        SSN = "230-54-8642",
                        EmailAddress = "jgreene18@gmail.com",
                        State = "VA",
                        DOB = new DateTime(2000, 6, 22),
                        Gender = "F",
                        GPA = 3.0M,
                        MathScore = 500,
                        VerbalScore = 600,
                        EnrollmentTerm = "Fall",
                        EnrollmentYear = DateTime.Now.Year
                    },
                    new Applicant() {
                        ApplicationDate = DateTime.Now.AddDays(-(int)DateTime.Today.DayOfWeek),
                        FirstName = "Nick",
                        LastName = "Carrington",
                        SSN = "224-58-9376",
                        EmailAddress = "ncarrington@gmail.com",
                        State = "VA",
                        DOB = new DateTime(1996, 5, 25),
                        Gender = "M",
                        GPA = 3.0M,
                        MathScore = 500,
                        VerbalScore = 600,
                        EnrollmentTerm = "Spring",
                        EnrollmentYear = DateTime.Now.Year
                    },
                    new Applicant() {
                        ApplicationDate = DateTime.Now,
                        FirstName = "Jen",
                        LastName = "Smith",
                        SSN = "219-10-3684",
                        EmailAddress = "jsmith19@gmail.com",
                        State = "MD",
                        DOB = new DateTime(2000, 1, 18),
                        Gender = "F",
                        GPA = 3.4M,
                        MathScore = 700,
                        VerbalScore = 600,
                        EnrollmentTerm = "Summer",
                        EnrollmentYear = DateTime.Now.Year
                    }
                };

                applicants.ForEach(c => context.Applicants.AddOrUpdate(a => a.ApplicantID, c));
            }
        }
    }
}