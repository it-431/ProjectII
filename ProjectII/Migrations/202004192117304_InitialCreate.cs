﻿namespace ProjectII.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Applicants",
                c => new
                    {
                        ApplicantID = c.Int(nullable: false, identity: true),
                        ApplicationDate = c.DateTime(nullable: false),
                        FirstName = c.String(nullable: false),
                        MiddleName = c.String(),
                        LastName = c.String(nullable: false),
                        SSN = c.String(nullable: false),
                        EmailAddress = c.String(nullable: false),
                        HomePhone = c.String(),
                        CellPhone = c.String(),
                        Street = c.String(),
                        City = c.String(),
                        State = c.String(),
                        Zipcode = c.String(),
                        DOB = c.DateTime(nullable: false),
                        Gender = c.String(),
                        HS = c.String(),
                        HSCity = c.String(),
                        GraduationDate = c.DateTime(),
                        GPA = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MathScore = c.Int(nullable: false),
                        VerbalScore = c.Int(nullable: false),
                        Major = c.String(),
                        EnrollmentTerm = c.String(),
                        EnrollmentYear = c.Int(),
                        EnrollmentDecision = c.String(),
                    })
                .PrimaryKey(t => t.ApplicantID);    
        }
        
        public override void Down()
        {
            DropTable("dbo.Applicants");
        }
    }
}