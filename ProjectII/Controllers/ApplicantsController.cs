﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjectII.Models;
using ProjectII.ViewModels;

namespace ProjectII.Controllers
{
    [Authorize]
    public class ApplicantsController : Controller
    {
        private ProjectIIContext db = new ProjectIIContext();

        // GET: Applicants
        public ActionResult Index(string search, string type)
        {
            // instantiate a new view model
            ApplicantViewModel viewModel = new ApplicantViewModel();
            var applicants = (IQueryable<Applicant>)db.Applicants;

            if (!String.IsNullOrEmpty(search))
            {
                switch (type)
                {
                    case "SSN":
                        applicants = applicants.Where(a => a.SSN.Contains(search));
                        break;
                    case "LastName":
                        applicants = applicants.Where(a => a.LastName.Contains(search));
                        break;
                }
                viewModel.search = search;
                viewModel.type = type;
            }

            viewModel.Applicants = applicants.OrderByDescending(a => a.ApplicationDate);

            if (Request.IsAjaxRequest())
            {
                return PartialView("ApplicantsPartial", viewModel);
            }

            return View(viewModel);
        }

        // GET: Applicants/Details/5
        [Route("applicant/{id}")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Applicant applicant = db.Applicants.Find(id);

            if (applicant == null)
            {
                return HttpNotFound();
            }
            return View(applicant);
        }

        // GET: Applicants/Create
        [AllowAnonymous]
        public ActionResult Create()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index");
            }
            return View();
        }

        // POST: Applicants/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public ActionResult Create([Bind(Include = "ApplicantID,ApplicationDate,FirstName,MiddleName,LastName,SSN,EmailAddress,HomePhone,CellPhone,Street,City,State,Zipcode,DOB,Gender,HS,HSCity,GraduationDate,GPA,MathScore,VerbalScore,Major,EnrollmentTerm,EnrollmentYear,EnrollmentDecision")] Applicant applicant)
        {
            if (ModelState.IsValid)
            {
                var rejected = false;
                Applicant DuplicateApplicant = db.Applicants.Where(cm => string.Compare(cm.SSN, applicant.SSN, true) == 0).FirstOrDefault();

                if (DuplicateApplicant != null)
                {
                    ModelState.AddModelError("", "The applicant has already applied.");
                    return View(applicant);
                }

                if (applicant.GPA < Constants.minGPA || applicant.MathScore + applicant.VerbalScore < Constants.minScore)
                {
                    applicant.EnrollmentDecision = "Reject";
                    rejected = true;
                }

                db.Applicants.Add(applicant);
                db.SaveChanges();

                if (rejected)
                {
                    return RedirectToAction("Rejected");
                }
                return RedirectToAction("Success", new { id = applicant.ApplicantID });
            }
            return View(applicant);
        }

        // GET: Applicants/Rejection/5
        [AllowAnonymous]
        [Route("application-rejected")]
        public ActionResult Rejected()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index");
            }

            return View();
        }

        // GET: Applicants/Success/5
        [AllowAnonymous]
        [Route("application/{id}")]
        public ActionResult Success(int? id)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index");
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Applicant applicant = db.Applicants.Find(id);

            if (applicant == null)
            {
                return HttpNotFound();
            }
            return View(applicant);
        }

        // GET: Applicants/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Applicant applicant = db.Applicants.Find(id);

            if (applicant == null)
            {
                return HttpNotFound();
            }
            return View(applicant);
        }

        // POST: Applicants/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ApplicantID,ApplicationDate,FirstName,MiddleName,LastName,SSN,EmailAddress,HomePhone,CellPhone,Street,City,State,Zipcode,DOB,Gender,HS,HSCity,GraduationDate,GPA,MathScore,VerbalScore,Major,EnrollmentTerm,EnrollmentYear,EnrollmentDecision")] Applicant applicant)
        {
            if (ModelState.IsValid)
            {
                db.Entry(applicant).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(applicant);
        }

        // GET: Applicants/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Applicant applicant = db.Applicants.Find(id);

            if (applicant == null)
            {
                return HttpNotFound();
            }
            return View(applicant);
        }

        // POST: Applicants/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Applicant applicant = db.Applicants.Find(id);
            db.Applicants.Remove(applicant);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}