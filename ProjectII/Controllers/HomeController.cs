﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectII.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [Route("about-mason")]
        public ActionResult About()
        {
            ViewBag.Message = "Patriots Bold & Brave";

            return View();
        }

        [Route("contact-us")]
        public ActionResult Contact()
        {
            return View();
        }
    }
}