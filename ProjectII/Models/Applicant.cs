﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectII.Models
{
    public class Applicant
    {
        [Display(Name = "Applicant ID")]
        public int ApplicantID { get; set; } 
        [Display(Name = "Application Date")]
        public DateTime ApplicationDate { get; set; }
        [Display(Name = "First Name")]
        [Required]
        public string FirstName { get; set; }
        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }
        [Display(Name = "Last Name")]
        [Required]
        public string LastName { get; set; }
        [Required]
        [RegularExpression(@"[0-9]{3}-[0-9]{2}-[0-9]{4}", ErrorMessage = "The SSN must be in the format AAA-GG-SSSS.")]
        public string SSN { get; set; }
        [Display(Name = "E-mail Address")]
        [Required]
        public string EmailAddress { get; set; }
        [Display(Name = "Home Number")]
        [RegularExpression(@"^([\(]{1}[0-9]{3}[\)]{1}[ |\-]{1}|^[0-9]{3}[\-| ])?[0-9]{3}(\-| ){1}[0-9]{4}$", ErrorMessage = "The Home Number must be in the format (xxx) xxx-xxxx).")]
        public string HomePhone { get; set; }
        [Display(Name = "Cell Number")]
        [RegularExpression(@"^([\(]{1}[0-9]{3}[\)]{1}[ |\-]{1}|^[0-9]{3}[\-| ])?[0-9]{3}(\-| ){1}[0-9]{4}$", ErrorMessage = "The Cell Number must be in the format (xxx) xxx-xxxx).")]
        public string CellPhone { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zipcode { get; set; }
        [Display(Name = "Date of Birth")]
        [DisplayFormat(DataFormatString = "{0:MMMM dd, yyyy}", ApplyFormatInEditMode = true)]
        [Required]
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        [Display(Name = "High School")]
        public string HS { get; set; }
        [Display(Name = "City")]
        public string HSCity { get; set; }
        [Display(Name = "Graduation Date")]
        [DisplayFormat(DataFormatString = "{0:MMMM dd, yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? GraduationDate { get; set; }
        [Range(0, 4)]
        public decimal GPA { get; set; }
        [Display(Name = "Math Score")]
        [Range(0, 800)]
        public int MathScore { get; set; }
        [Range(0, 800)]
        [Display(Name = "Verbal Score")]
        public int VerbalScore { get; set; }
        public string Major { get; set; }
        [Display(Name = "Enrollment Term")]
        public string EnrollmentTerm { get; set; }
        [Display(Name = "Enrollment Year")]
        public int? EnrollmentYear { get; set; }
        [Display(Name = "Enrollment Decision")]
        public string EnrollmentDecision { get; set; }
    }
}