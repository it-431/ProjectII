﻿using ProjectII.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectII.ViewModels
{
    public class ApplicantViewModel
    {
        public IQueryable<Applicant> Applicants { get; set; }
        public string search { get; set; }
        public string type { get; set; }
    }
}