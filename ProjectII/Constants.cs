﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectII
{
    public static class Constants
    {
        public const int PageSize = 10;
        public const decimal minGPA = 3.0M;
        public const int minScore = 1000;

        public static List<SelectListItem> Genders
        {
            get
            {
                var genders = new List<SelectListItem>();

                genders.Add(new SelectListItem
                {
                    Value = "F",
                    Text = "Female"
                });
                genders.Add(new SelectListItem
                {
                    Value = "M",
                    Text = "Male"
                });

                return genders;
            }
        }

        public static List<SelectListItem> States
        {
            get
            {
                var names = new List<String>();
                names.Add("Alabama");
                names.Add("Alaska");
                names.Add("Arizona");
                names.Add("Arkansas");
                names.Add("California");
                names.Add("Colorado");
                names.Add("Connecticut");
                names.Add("Delaware");
                names.Add("District of Columbia");
                names.Add("Florida");
                names.Add("Georgia");
                names.Add("Hawaii");
                names.Add("Idaho");
                names.Add("Illinois");
                names.Add("Indiana");
                names.Add("Iowa");
                names.Add("Kansas");
                names.Add("Kentucky");
                names.Add("Louisiana");
                names.Add("Maine");
                names.Add("Maryland");
                names.Add("Massachusetts");
                names.Add("Michigan");
                names.Add("Minnesota");
                names.Add("Mississippi");
                names.Add("Missouri");
                names.Add("Montana");
                names.Add("Nebraska");
                names.Add("Nevada");
                names.Add("New Hampshire");
                names.Add("New Jersey");
                names.Add("New Mexico");
                names.Add("New York");
                names.Add("North Carolina");
                names.Add("North Dakota");
                names.Add("Ohio");
                names.Add("Oklahoma");
                names.Add("Oregon");
                names.Add("Pennsylvania");
                names.Add("Rhode Island");
                names.Add("South Carolina");
                names.Add("South Dakota");
                names.Add("Tennessee");
                names.Add("Texas");
                names.Add("Utah");
                names.Add("Vermont");
                names.Add("Virginia");
                names.Add("Washington");
                names.Add("West Virginia");
                names.Add("Wisconsin");
                names.Add("Wyoming");

                var abbreviations = new List<String>();
                abbreviations.Add("AL");
                abbreviations.Add("AK");
                abbreviations.Add("AZ");
                abbreviations.Add("AR");
                abbreviations.Add("CA");
                abbreviations.Add("CO");
                abbreviations.Add("CT");
                abbreviations.Add("DE");
                abbreviations.Add("DC");
                abbreviations.Add("FL");
                abbreviations.Add("GA");
                abbreviations.Add("HI");
                abbreviations.Add("ID");
                abbreviations.Add("IL");
                abbreviations.Add("IN");
                abbreviations.Add("IA");
                abbreviations.Add("KS");
                abbreviations.Add("KY");
                abbreviations.Add("LA");
                abbreviations.Add("ME");
                abbreviations.Add("MD");
                abbreviations.Add("MA");
                abbreviations.Add("MI");
                abbreviations.Add("MN");
                abbreviations.Add("MS");
                abbreviations.Add("MO");
                abbreviations.Add("MT");
                abbreviations.Add("NE");
                abbreviations.Add("NV");
                abbreviations.Add("NH");
                abbreviations.Add("NJ");
                abbreviations.Add("NM");
                abbreviations.Add("NY");
                abbreviations.Add("NC");
                abbreviations.Add("ND");
                abbreviations.Add("OH");
                abbreviations.Add("OK");
                abbreviations.Add("OR");
                abbreviations.Add("PA");
                abbreviations.Add("RI");
                abbreviations.Add("SC");
                abbreviations.Add("SD");
                abbreviations.Add("TN");
                abbreviations.Add("TX");
                abbreviations.Add("UT");
                abbreviations.Add("VT");
                abbreviations.Add("VA");
                abbreviations.Add("WA");
                abbreviations.Add("WV");
                abbreviations.Add("WI");
                abbreviations.Add("WY");

                var states = new List<SelectListItem>();

                for (var i = 0; i < names.Count; i++)
                {
                    states.Add(new SelectListItem
                    {
                        Value = abbreviations[i],
                        Text = names[i]
                    });
                }

                return states;
            }
        }
        public static SelectList Statuses
        {
            get
            {
                return new SelectList(new List<String> { "Admit", "Reject", "Wait List" });
            }
        }

        public static SelectList Terms
        {
            get
            {
                return new SelectList(new List<String> { "Fall", "Spring", "Summer" });
            }
        }

        public static SelectList Majors
        {
            get
            {
                var majors = new List<String>();
                majors.Add("Animation, Interactive Technology, Video Graphics and Special Effects");
                majors.Add("Computer and Information Sciences");
                majors.Add("Computer and Information Sciences");
                majors.Add("Information Technology");
                majors.Add("Computer Science");
                majors.Add("Education");
                majors.Add("Health Teacher Education");
                majors.Add("Physical Education Teaching and Coaching");
                majors.Add("Engineering");
                majors.Add("Civil Engineering");
                majors.Add("Computer Engineering");
                majors.Add("Electrical and Electronics Engineering");
                majors.Add("Systems Engineering");
                majors.Add("Foreign Languages");
                majors.Add("Foreign Languages and Literatures");
                majors.Add("English and Literature");
                majors.Add("English Language and Literature");
                majors.Add("Rhetoric and Composition");
                majors.Add("Liberal Arts and Humanities");
                majors.Add("Liberal Arts and Sciences/Liberal Studies");
                majors.Add("Liberal Arts and Sciences Studies and Humanities, Other");
                majors.Add("Biology/Biological Sciences");
                majors.Add("Neuroscience");
                majors.Add("Mathematics");
                majors.Add("Mathematics");
                majors.Add("General Studies");
                majors.Add("Peace Studies and Conflict Resolution");
                majors.Add("Mathematics and Computer Science");
                majors.Add("Philosophy and Religious Studies");
                majors.Add("Philosophy");
                majors.Add("Religion/Religious Studies");
                majors.Add("Physical Sciences");
                majors.Add("Astronomy");
                majors.Add("Chemistry");
                majors.Add("Geology/Earth Science");
                majors.Add("Physics");
                majors.Add("Physical Sciences, Other");
                majors.Add("Psychology");
                majors.Add("Homeland Security, Law Enforcement, Firefighting");
                majors.Add("Criminal Justice/Police Science");
                majors.Add("Human Services");
                majors.Add("Public Administration");
                majors.Add("Social Work");
                majors.Add("Social Sciences");
                majors.Add("Anthropology");
                majors.Add("Economics");
                majors.Add("Geography");
                majors.Add("International Relations and Affairs");
                majors.Add("Political Science and Government, Other");
                majors.Add("Sociology");
                majors.Add("Ethnic Studies");
                majors.Add("Latin American Studies");
                majors.Add("Russian Studies");
                majors.Add("Area Studies, Other");
                majors.Add("Visual and Performing Arts");
                majors.Add("Dance");
                majors.Add("Drama and Dramatics/Theatre Arts");
                majors.Add("Cinematography and Film/Video Production");
                majors.Add("Art/Art Studies");
                majors.Add("Art History, Criticism and Conservation");
                majors.Add("Music Performance");
                majors.Add("Athletic Training/Trainer");
                majors.Add("Clinical Laboratory Science/Medical Technology/Technologist");
                majors.Add("Community Health and Preventive Medicine");
                majors.Add("Registered Nursing/Registered Nurse");
                majors.Add("Health Professions and Related Clinical Sciences, Other");
                majors.Add("Business");
                majors.Add("Business Administration and Management");
                majors.Add("Accounting");
                majors.Add("Finance");
                majors.Add("Tourism and Travel Services Management");
                majors.Add("Management Sciences and Quantitative Methods, Other");
                majors.Add("Marketing/Marketing Management");
                majors.Add("History");
                majors.Sort();

                return new SelectList(majors);
            }
        }
    }
}